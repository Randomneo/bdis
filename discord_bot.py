'''
Discord bot for playing music and some extra stuf
website for more info github.com/Randomneo/discord_chatbot

Developed my Misiura Rostislav
'''

import discord
import asyncio
import random as rand
import vk as VK

from config import token 
from config import voice_channel_id 
from config import debug_message_channel_id 
from config import mixs_file 

help_message="""
This is help for using discord chat bot
!join - подключиться к голосовому каналу(нужно что бы возпроизводить музыку)
!play <link or nameOfMix> - возпроизвести музыку
!resume - возобновить
!pause - пауза
!stop - остановить музыку(нужно обязательно останавливать перед возпроизведением нового трека)
!mem - показать мем
Test - send test message
"""

def load_mixs():
    global music_mix
    with open(mixs_file, 'r') as list_file:
        for line in list_file:
            name = line.split(' ')[0]
            mix_link = line.split(' ')[1]
            if '\n' in mix_link:
                mix_link = mix_link[:-1]
            music_mix[name] = mix_link

def save_mixs():
	global music_mix
	print("save list")
	l = []
	keys = list(music_mix.keys())
	values = list(music_mix.values())
	resstr = ""
	for i in range(len(music_mix)):
		resstr = resstr + keys[i] + " " + values[i] + '\n'
	resstr = resstr[:-1]
	with open(mixs_file, 'w') as list_file:
		list_file.write(resstr)

class dbot:
	"""
	Basic discord bot
	with music playback support
	"""
	
	global client
	global vk
	global music_mix
	global debug_channel
	
		
	vk = VK.API(VK.Session())
	client = discord.Client()
	
	@client.event
	async def on_ready():
		"""
		Send message when client is ready for do some stuff
		"""
		
		print("Client is runned")
		debug_channel = client.get_channel(debug_message_channel_id)
		await client.send_message(debug_channel,"Client is runned")
		
		global pubname
		global bayan
		pubname = "4ch_2ch"
		bayan = 0

		
	@client.event
	async def on_message(message):
		"""
		Analyze message content and react on this message
		"""
		
		global voice
		global current_link
		global player
		global pubname
		global bayan	

		msst = message.content.startswith
		async def sendms(text):
			await client.send_message(message.channel, text)
	
	
		if(msst("Test")):
			await sendms("test")
		elif(msst("!join")):
			channel = client.get_channel(voice_channel_id)
			voice = await client.join_voice_channel(channel)
		elif(msst("!play")):
			link = message.content.split(' ')[1]
			if not link.startswith('http'):
				link = music_mix[link]
			current_link = link
			player = await voice.create_ytdl_player(link)
			player.start()
		elif(msst("!stop")):
			player.stop()
		elif(msst("!pause")):
			player.pause()
		elif(msst("!resume")):
			player.resume()
		elif(msst("!setvolume")):
			newvolume = float(message.content.split(" ")[1])
			player.volume = newvolume
		elif(msst("!mem")):
			post = vk.wall.get(domain = pubname, count = 1, offset = bayan)
			mem = post[1]['attachment']['photo']['src_big']
			await sendms(mem)
			bayan = bayan + 1
		elif(msst("!help")):
			await sendms(help_message)
			
		elif("lol" in message.content):
			await sendms("Тебе смешьно а мне нет. Служить такому дауну как ты.")
		elif("Альтрон" in message.content and "даун?" in message.content):
			if(not "Рост" in message.content and not "Мисюра" in message.content):
				await sendms("Да")
			else:
				await sendms("Нет")
			
				
		
	def run(self):
		"""
		init and run client with token from config.py
		"""
		
		client.run(token)



if __name__ == '__main__':
	load_mixs()
	dbot().run()
	save_mixs()
